/**
 * Created by bouh on 12/01/17.
 */
public class LinkedListHandler<T>{
    LinkedList<T> head;
    LinkedList<T> tail;
    LinkedList<T> list;
    int size;

    /*
    *** Constructors
    *** ------------
     */

    public LinkedListHandler(LinkedList<T> head, LinkedList<T> tail, LinkedList<T> list){
        this.head = head;
        this.tail = goToTail();
        this.list = list;
        this.size = size();
    }

    public LinkedListHandler(){
        this(null, null, null);
    }

    /*
    *** Methods
    *** -------
     */

    private int size(){
        int pos = 0;
        this.list = head;
        while (this.list != null){
            pos++;
            this.list = this.list.getNext();
        }
        return pos;
    }

    private LinkedList<T> goToTail(){
        LinkedList<T> next;

        if (this.list == null)
            this.list = head;
        if (this.list == null)
            return null;
        next = this.list.getNext();
        while (next != null){
            this.list = next;
            next = this.list.getNext();
        }
        return this.list;
    }

    private void createNext(T content){
        LinkedList<T> elem = new LinkedList(content, null, list);
        if (this.list.getNext() == null)
            this.tail = elem;
        this.list.insertNext(elem);
        this.size++;
    }

    private void createPrev(T content){
        LinkedList<T> elem = new LinkedList(content, list);
        if (this.list.getPrev() == null)
            this.head = elem;
        this.list.insertPrev(elem);
        this.size++;
    }

    private void deleteNext(){
        if (this.list != null)
            this.list.deleteNext();
        this.size--;
    }

    private void deletePrev(){
        if (this.list != null)
            this.list.deletePrev();
        this.size--;
    }

    public T get(int id){
        int ct = 0;

        this.list = head;
        while (this.list != null)
        {
            if (ct++ == id)
                return list.getContent();
            this.list = this.list.getNext();
        }
        return null;
    }

    public T add(T content, int pos){
        int ct = 0;

        if (head == null){
            this.head = new LinkedList<T>(content);
            this.tail = this.head;
            this.list = this.head;
            return this.list.getContent();
        }
        this.list = head;
        while (this.list != null )
        {
            if (ct++ == pos) {
                createPrev(content);
                return this.list.getPrev().getContent();
            }
            this.list = this.list.getNext();
        }
        return null;
    }

    public boolean remove(int pos){
        int ct = 0;

        if (head == null){
            return false;
        }
        this.list = head;
        while (this.list != null )
        {
            if (ct++ == pos + 1) {
                deletePrev();
                return true;
            }
            this.list = this.list.getNext();
        }
        return false;
    }
}
