/**
 * Created by bouh on 12/01/17.
 */
public class ListHandler<T> {

    /*
    *** values:
    *** _______ "ArrayList"
    *** _______ "LinkedList"
     */
    private String currentListType;
    private LinkedListHandler<T> LL_list;
    private ArrayListHandler<T> AL_list;

    /*
    *** Constructors
    *** ------------
     */

    public ListHandler(String ListType){
        if (isValidListType(ListType)){
            this.currentListType = ListType;
        } else {
            this.currentListType = "ArrayList";
        }
    }

    /*
    *** Methods
    *** -------
     */

    private boolean isValidListType(String type){
        switch (type) {
            case "ArrayList":
                return true;
            case "LinkedList":
                return true;
            default:
                return false;
        }
    }

    public boolean switchListType(String type){
        if (!isValidListType(type)){
            return false;
        }
        //need to register one list to another based on th currently used.

        return true;
    }

    private boolean switchToAL_list(){
        return true;
    }

    private boolean switchToLL_list(){
        return true;
    }

    /*
    *** LL_list
     */

    private LinkedListHandler<T> getLL_list() {
        return LL_list;
    }

    private void setLL_list(LinkedListHandler<T> LL_list) {
        this.LL_list = LL_list;
    }

    /*
    *** AL_list
     */

    private ArrayListHandler<T> getAL_list() {
        return AL_list;
    }

    private void setAL_list(ArrayListHandler<T> AL_list) {
        this.AL_list = AL_list;
    }
}
