/**
 * Created by bouh on 12/01/17.
 */
public class LinkedList<T> {
    private LinkedList<T> prev;
    private LinkedList<T> next;
    private T content;

    /*
    *** Constructors
    *** ------------
     */

    public LinkedList(T content, LinkedList prev, LinkedList next) {
        this.content = content;
        this.prev = prev;
        this.next = next;

    }

    public LinkedList(T content, LinkedList prev) {
        this(content, prev, null);

    }

    public LinkedList(T content){
        this(content, null, null);
    }

    public LinkedList(){
        this(null, null, null);
    }

    /*
    *** Methods
    *** -------
     */

    public void reset(){
        this.content = null;
        this.next = null;
        this.prev = null;
    }

    /*
    *** Next
     */

    public LinkedList<T> getNext() {
        return next;
    }

    public void setNext(LinkedList<T> next) {
        this.next = next;
    }

    public void insertNext(LinkedList<T> next) {
        LinkedList<T> tmp = this.next;
        this.next = next;
        if (tmp != null)
            tmp.setPrev(next);
        if (next != null) {
            next.setNext(tmp);
            next.setPrev(this);
        }
    }

    public void deleteNext() {
        LinkedList<T> tmp = this.next;
        if (tmp != null) {
            this.next = tmp.getNext();
            if (tmp.getNext() != null){
                tmp.getNext().setPrev(this);
            }
            tmp.reset();
        }
        else
            this.next = null;
    }

    /*
    *** Prev
     */

    public LinkedList<T> getPrev() {
        return prev;
    }

    public void setPrev(LinkedList prev) {
        this.prev = prev;
    }

    public void insertPrev(LinkedList prev) {
        LinkedList<T> tmp = this.prev;
        this.prev = prev;
        if (tmp != null)
            tmp.setNext(prev);
        if (prev != null) {
            prev.setPrev(tmp);
            prev.setNext(this);
        }
    }

    public void deletePrev() {
        LinkedList<T> tmp = this.prev;
        if (tmp != null) {
            this.prev = tmp.getPrev();
            if (tmp.getPrev() != null){
                tmp.getPrev().setNext(this);
            }
            tmp.reset();
        }
        else
            this.prev = null;
    }

    /*
    *** Content
     */

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
