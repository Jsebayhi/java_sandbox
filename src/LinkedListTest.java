import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.*;

/**
 * Created by bouh on 12/01/17.
 */
public class LinkedListTest {

    private Object[] expected;
    private LinkedList[] prev;
    private LinkedList[] next;

    @Parameterized.Parameters
    public static Collection<Object[]> testConditions(){
        return Arrays.asList(new Object[][][] {
                {{null, null, null}, {true}, {null}}
        });
    }

    public LinkedListTest(Object[] expected, LinkedList[] prev, LinkedList[] ) {
        this.content = content;
        this.prev = prev;
        this.next = next;
    }

    @org.junit.Test
    public void getNext() throws Exception {
        LinkedList list;

        list = new LinkedList(null, prev, next);
        assertEquals(null, list.getNext());
        assertEquals(null, list.getPrev());
        assertEquals(null, list.getContent());
        list = null;
    }


}